# Playground for "Ansible for DevOps"

A learning environment for "[Ansible for DevOps: Write your first Playbook](https://www.coursera.org/projects/ansible-write-your-first-playbook-and-learn-how-it-works)" in a single machine. It requires only [docker-compose](https://docs.docker.com/compose/).

```sh
# Check the version of docker compose.
$ docker compose version
Docker Compose version v2.5.1

# Start up all containers.
$ docker compose up -d

# Set root password in all server containers.
$ docker compose exec server1 passwd
$ docker compose exec server2 passwd

# In controller container.
$ docker compose exec controller bash

# Generate a key-pair and copy one to servers.
root@controller:~# ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519 -N ""
root@controller:~# ssh-copy-id -i ~/.ssh/id_ed25519.pub server1
root@controller:~# ssh-copy-id -i ~/.ssh/id_ed25519.pub server2

# Modify an ansible hosts file.
root@controller:~# echo "[servers]
server1
server2" >> /etc/ansible/hosts

# Play the playbook.
root@controller:~# ansible-playbook /workspace/apache.yml

# Check heath for Apache servers.
root@controller:~# curl server1
root@controller:~# curl server2

# Uncomment statements in apache.yml to change a port of Apache,
# described in Part2.

# Replay the playbook to change the port.
root@controller:~# curl server1:81
root@controller:~# curl server2:81

# Cleanup
root@controller:~# exit
$ docker compose down
```
